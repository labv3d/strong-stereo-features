# Strong Stereo Features for Self-Supervised Practical Stereo Matching

## Files
Both notebooks can be used with Mathematica 13.2.0.

### Training-Notebook.nb
This file contains the mathematica code used to create and train the self-supervised network as well as build the inference network.

### Testing-Notebook.nb
This file contains the mathematica code for all experiments. 

### opencv-4.5.4/modules/calib3s/src/stereosgbm.cpp 
This file houses the modifications to openCV's stereosgbm.cpp.

### stereo.cpp
This file contains the cxx code that generates matching cost volumes with Ort library and fed to openCV.

### kitti15.onnx 
This file is an example trained model on kitti15 training set, used for results online.

# Installation
## Opencv
Download the sources for openCV-4.5.4 at https://opencv.org/opencv-4-5-4/

Download the sources for opencv_contrib-4.x at https://github.com/opencv/opencv_contrib/archive/4.x.zip

Replace the stereosgbm.cpp in the sources with the provided one in opencv-4.5.4.

Build from source with opencv.sh.

## OnnxRuntime
git clone --recursive https://github.com/Microsoft/onnxruntime

cd onnxruntime

./build.sh --config RelWithDebInfo --build_shared_lib --parallel

## Stereo
cmake --build .

make

# Usage
Both notebooks can be read and executed with Mathematica 13.2.0.

Change line 415 and 419 of sgbm.cpp for your input directory.

Change line 475 of sgbm.cpp for your output directory.

SGBM parameters and penalties are on lines 241 to 253 of sgbm.cpp. 

Change line 2 of mathNN.m to specify the trained network location.

Recompile stereo.cpp after each modification.