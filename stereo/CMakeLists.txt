# Copyright (c) Microsoft Corporation. All rights reserved.
# Licensed under the MIT License.

cmake_minimum_required(VERSION 3.13)

project(inference C CXX)

string(APPEND CMAKE_CXX_FLAGS " -Wall -Wextra -Wno-unused-variable")
string(APPEND CMAKE_C_FLAGS " -Wall -Wextra -Wno-unused-variable")

set(CMAKE_CXX_STANDARD 17)


#onnxruntime  providers
#option(onnxruntime_USE_CUDA "Build with CUDA support" ON)


set(ONNXRUNTIME_ROOTDIR "/home/roys/src/onnxruntime/onnxruntime" CACHE STRING "onnxruntime root dir")
include_directories("${ONNXRUNTIME_ROOTDIR}/include" "${ONNXRUNTIME_ROOTDIR}/include/onnxruntime/core/session")
#link_directories("${ONNXRUNTIME_ROOTDIR}/lib")
link_directories("${ONNXRUNTIME_ROOTDIR}/build/Linux/RelWithDebInfo")


#if(onnxruntime_USE_CUDA)
#  add_definitions(-DUSE_CUDA)
#endif()


find_package( OpenCV REQUIRED )
include_directories( ${OpenCV_INCLUDE_DIRS} )



add_executable(stereo "stereo.cpp"  "mononnx.cpp")
target_link_libraries(stereo onnxruntime ${OpenCV_LIBS})


