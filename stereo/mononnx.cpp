

#include "mononnx.h"

#include <assert.h>
#include <stdio.h>

//#include "onnxruntime_c_api.h"
// pour les  options  de  session
//#include "onnxruntime_session_options_config_keys.h"


const OrtApi* onnx=NULL;

// pour les strings,  un peu partout
OrtAllocator* allocator;

// partage entre  toutes les sessions...
OrtEnv* env;
OrtSessionOptions* session_options;


OrtMemoryInfo* memory_info_cpu;
OrtMemoryInfo* memory_info_cuda;


#include <sys/time.h>

//
// heure en secondes, precise a la  microseconde
//

double horloge() {
    struct timeval tv;
    gettimeofday(&tv,0L);
    double h=(double)tv.tv_sec+(double)tv.tv_usec/1000000.0; 
    return(h);
}


//
// dump info  sur une  session  (un graphe)
//

//
// info  sur un type de tenseur et type de donnee
//
void  dumpTypeInfo(OrtTypeInfo *ty) {
    if( ty==NULL ) {  printf("[[no  typeinfo]]\n");return;  }
    const OrtTensorTypeAndShapeInfo* tensor_info;
    TRY(onnx->CastTypeInfoToTensorInfo(ty, &tensor_info));
    ONNXTensorElementDataType type;
    size_t ndims;
    int64_t dims[50];
    TRY(onnx->GetTensorElementType(tensor_info, &type));
    TRY(onnx->GetDimensionsCount(tensor_info, &ndims))
    TRY(onnx->GetDimensions(tensor_info, dims, ndims));
    printf("           : tensor dims=%d [",(int)ndims);
    for(int j=0;j<(int)ndims;j++)  printf("%d,",(int)dims[j]);
    printf("]  type=%d (FLOAT=%d,int64=%d,...)\n",type,
            ONNX_TENSOR_ELEMENT_DATA_TYPE_FLOAT,
            ONNX_TENSOR_ELEMENT_DATA_TYPE_INT64);
}

// va chercher la liste des  dimensions  de  ce type... termine par un  -1.
void getTypeInfoDim(OrtTypeInfo *ty,int  dimensions[])  {
    dimensions[0]=-1;
    if( ty==NULL ) {  printf("[[no  typeinfo]]\n");return;  }
    const OrtTensorTypeAndShapeInfo* tensor_info;
    TRY(onnx->CastTypeInfoToTensorInfo(ty, &tensor_info));
    ONNXTensorElementDataType type;
    size_t ndims;
    int64_t dims[50];
    TRY(onnx->GetTensorElementType(tensor_info, &type));
    TRY(onnx->GetDimensionsCount(tensor_info, &ndims))
    TRY(onnx->GetDimensions(tensor_info, dims, ndims));
    int i;
    for(i=0;i<(int)ndims;i++) dimensions[i]=(int)dims[i];
    dimensions[i]=-1; //  last!
}

void getDimensions(OrtSession *session,const  char *name,int *dimensions) {

    size_t count;
    int  inCount,outCount,overCount;
    TRY(onnx->SessionGetInputCount(session, &count));inCount=(int)count;
    TRY(onnx->SessionGetOutputCount(session, &count));outCount=(int)count;
    TRY(onnx->SessionGetOverridableInitializerCount(session,&count));overCount=(int)count;

    OrtTypeInfo* typeinfo;
    char *n;
    // dans les inputs!
    for(int  i=0;i<inCount;i++)  {
        TRY(onnx->SessionGetInputName(session, i, allocator, &n));
        if( strcmp(n,name)!=0 ) continue;
        printf("*** input [%2d] : %s\n",i,n);

        TRY(onnx->SessionGetInputTypeInfo(session, i, &typeinfo));
        getTypeInfoDim(typeinfo,dimensions);
        if (typeinfo) onnx->ReleaseTypeInfo(typeinfo);
        return;
    }
    // dans les outputs!
    for(int  i=0;i<outCount;i++)  {
        TRY(onnx->SessionGetOutputName(session, i, allocator, &n));
        if( strcmp(n,name)!=0 ) continue;
        printf("*** output [%2d] : %s\n",i,n);

        TRY(onnx->SessionGetOutputTypeInfo(session, i, &typeinfo));
        getTypeInfoDim(typeinfo,dimensions);
        if (typeinfo) onnx->ReleaseTypeInfo(typeinfo);
        return;
    }
    // dans  les initializers!
    for(int  i=0;i<overCount;i++)  {
        TRY(onnx->SessionGetOverridableInitializerName(session,i,allocator,&n));
        if( strcmp(n,name)!=0 ) continue;
        printf("*** initializer [%2d] : %s\n",i,n);

        TRY(onnx->SessionGetOverridableInitializerTypeInfo(session,i, &typeinfo));
        getTypeInfoDim(typeinfo,dimensions);
        if (typeinfo) onnx->ReleaseTypeInfo(typeinfo);
        return;
    }
    dimensions[0]=-1;
    return;
}



//
// information  sur un  tenseur
//
void  dumpTensorInfo(OrtValue *tensor) {
    OrtTypeInfo* typeinfo;
    TRY(onnx->GetTypeInfo(tensor, &typeinfo));
    dumpTypeInfo(typeinfo);
    if (typeinfo) onnx->ReleaseTypeInfo(typeinfo);
}

//
// Information sur une session
//
void dumpSessionInfo(OrtSession* session) {

    size_t count;
    int inCount,outCount;

    TRY(onnx->SessionGetInputCount(session, &count));inCount=(int)count;
    TRY(onnx->SessionGetOutputCount(session, &count));outCount=(int)count;

    printf("In  count: %d\n",inCount);
    printf("Out count: %d\n",outCount);

    char *name;
    for(int  i=0;i<inCount;i++)  {
        TRY(onnx->SessionGetInputName(session, i, allocator, &name));
        printf("input [%2d] : %s\n",i,name);

        OrtTypeInfo* typeinfo;
        TRY(onnx->SessionGetInputTypeInfo(session, i, &typeinfo));
        dumpTypeInfo(typeinfo);
        if (typeinfo) onnx->ReleaseTypeInfo(typeinfo);
    }

    for(int  i=0;i<outCount;i++)  {
        TRY(onnx->SessionGetOutputName(session, i, allocator, &name));
        printf("output [%2d] : %s\n",i,name);
        OrtTypeInfo* typeinfo;
        TRY(onnx->SessionGetOutputTypeInfo(session, i, &typeinfo));
        dumpTypeInfo(typeinfo);
        if (typeinfo) onnx->ReleaseTypeInfo(typeinfo);
    }

    //  override  initializers?
    int overCount;
    TRY(onnx->SessionGetOverridableInitializerCount(session,&count));overCount=(int)count;
    printf("** overridable is %d\n",overCount);

    for(int i=0;i<overCount;i++) {
        TRY(onnx->SessionGetOverridableInitializerName(session,i,allocator,&name));

        OrtTypeInfo *ti;
        TRY(onnx->SessionGetOverridableInitializerTypeInfo(session,i, &ti));
        printf("[%2d] %30s ",i,name);dumpTypeInfo(ti);
        if (ti) onnx->ReleaseTypeInfo(ti);
    }

}




int enable_cuda(OrtSessionOptions* session_options) {
    // OrtCUDAProviderOptions is a C struct.
    OrtCUDAProviderOptions o;
    memset((unsigned char *)&o, 0, sizeof(o)); // init to 0
    o.cudnn_conv_algo_search = OrtCudnnConvAlgoSearchExhaustive;
    o.gpu_mem_limit = SIZE_MAX;

    OrtStatus* onnx_status = onnx->SessionOptionsAppendExecutionProvider_CUDA(session_options, &o);
    if (onnx_status != NULL) {
        const char* msg = onnx->GetErrorMessage(onnx_status);
        fprintf(stderr, "%s\n", msg);
        onnx->ReleaseStatus(onnx_status);
        return -1;
    }
    return 0;
}


// return 0 si using CPU, 1  si  using  CUDA
int initONNX(int useCuda) {
    onnx = OrtGetApiBase()->GetApi(ORT_API_VERSION);
    if (!onnx) { fprintf(stderr, "Failed to init ONNX Runtime engine.\n"); abort(); }

    TRY(onnx->CreateEnv(ORT_LOGGING_LEVEL_WARNING, "test", &env));

    TRY(onnx->DisableTelemetryEvents(env)); // Merci, Microsoft!

    TRY(onnx->CreateSessionOptions(&session_options));

    if( useCuda )  {
        int k=enable_cuda(session_options);
        if( k ) {fprintf(stderr,"CUDA is not available. Using CPU\n");useCuda=0; }
        else    fprintf(stderr,"CUDA is enabled\n");
    }

    // generic  string allocator
    TRY(onnx->GetAllocatorWithDefaultOptions(&allocator));

    // memory
    TRY(onnx->CreateMemoryInfo("Cpu",OrtArenaAllocator, 0,OrtMemTypeDefault, &memory_info_cpu));
    TRY(onnx->CreateMemoryInfo("Cuda",OrtArenaAllocator, 0,OrtMemTypeDefault, &memory_info_cuda));

    return(useCuda);
}

void  uninitONNX()  {
    onnx->ReleaseMemoryInfo(memory_info_cpu);
    onnx->ReleaseMemoryInfo(memory_info_cuda);
    onnx->ReleaseSessionOptions(session_options);
    onnx->ReleaseEnv(env);
}


OrtSession* readONNX(const char *name)  {
    OrtSession* session;
    TRY(onnx->CreateSession(env, name, session_options, &session));
    return(session);
}


////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////
#ifdef SKIPALL


//
// session ne sert  qu'a donner un  reseau... pas a  allouer  des tenseurs
// run  session 1 puis  session2  un  certain nombre  de  fois
//
int inference( OrtSession* session1, OrtSession* session2)  {

    //
    // momeoire CPU et GPU
    //


    //
    // cree  un tenseur [270,480,480]
    //

    const int64_t shape[] = {270,480,480};
    int  sz=270*480*480;
    float *data=(float *)malloc(sz*sizeof(float));
    for(int i=0;i<sz;i++) data[i]=0.0;
    data[0]=1.0;

    OrtValue* inTensor=NULL;
    TRY(onnx->CreateTensorWithDataAsOrtValue(memory_info_cpu, data,sz*sizeof(float), shape,3, ONNX_TENSOR_ELEMENT_DATA_TYPE_FLOAT, &inTensor));
    assert(inTensor!=NULL);

    //  pourramasser  des  tensors values  et  names. En CPU.
    OrtAllocator* allocator;
    TRY(onnx->GetAllocatorWithDefaultOptions(&allocator));


    ////////////////////////////////////////////////

    double h0,h1,h2;

    //
    // i=0:  inTensor -> session1  -> out1
    // i=1:  out1 -> session2  -> out1 (ou out2 i==n-1)
    // ...
    //
    //
    OrtIoBinding* binding1;
    TRY(onnx->CreateIoBinding(session1, &binding1));

    OrtValue** outTensors1; // [count]
    size_t  count1;
    OrtValue** outTensors2; // [count]
    size_t  count2;

    double  sum=0.0;
    h0=horloge();
    int  nb=40;
    for(int i=0;i<nb;i++)  {
        h1=horloge();

        onnx->ClearBoundInputs(binding1);
        onnx->ClearBoundOutputs(binding1);

        //
        // inputs
        //

        if(  i==0 )  {
            TRY(onnx->BindInput(binding1, "Input", inTensor));
        }else{
            TRY(onnx->BindInput(binding1, "Input", outTensors1[0]));
        }

        //
        //  outputs
        //

        if( i==0 )  {
            // create outTensors1
            TRY(onnx->BindOutputToDevice(binding1,"Output", memory_info_cuda)); //_cuda
        }else if( i<nb-1  ) {
            //  use  outTensors1
            TRY(onnx->BindOutput(binding1,"Output", outTensors1[0]));
        }else{
            //  create new  output, outTensors2
            TRY(onnx->BindOutputToDevice(binding1,"Output", memory_info_cpu));
        }

        //  seulement seesion1
        TRY(onnx->RunWithBinding((i==0||1)?session1:session2,NULL,binding1));

        h2=horloge();
        printf("temps=%12.6f\n",h2-h1);
        sum+=(h2-h1);

        //  output values
        if( i==0 )  {
            TRY(onnx->GetBoundOutputValues(binding1,allocator, &outTensors1, &count1));
        }else if(  i==nb-1  )  {
            TRY(onnx->GetBoundOutputValues(binding1,allocator, &outTensors2, &count2));
        }

    }

    h2=horloge();
    printf("temps=%12.6f  calcul=%12.6f par iteration %12.6fms\n",h2-h0,sum,sum*1000./nb);

    float *ddd=NULL;
    TRY(onnx->GetTensorMutableData(outTensors2[0], (void**)&ddd));
    printf("ddd=0x%08lx\n",(long  int)ddd);
    for(int  i=0;i<20;i++) printf("[%3d]  %12.6f\n",i,ddd[i]);

    onnx->ReleaseValue(outTensors1[0]);
    onnx->ReleaseValue(outTensors2[0]);

    onnx->ReleaseIoBinding(binding1);

    //TRY(onnx->IsTensor(output_tensors[0], &is_tensor));assert(is_tensor);

    // change le tenseur...
    //output_tensor_data[0]=1.0;

    // run2!
    //TRY(onnx->Run(session2, NULL, input_names, output_tensors, 1, output_names, 1,output2_tensors));

    //TRY(onnx->IsTensor(output2_tensors[0], &is_tensor));assert(is_tensor);

    /*
    h1=horloge();
    TRY(onnx->Run(session2, NULL, input_names,output_tensors,1,output_names, 1,input_tensors));
    h2=horloge();
    printf("temps=%12.6f tensor=0x%08lx\n",h2-h1,(long int)output_tensors[0]);
    */

    //TRY(onnx->GetTensorMutableData(output_tensors[0], (void**)&output_tensor_data));
    //TRY(onnx->GetTensorMutableData(output2_tensors[0], (void**)&output_tensor2_data));

    //printf(" tensor data 0x%08lx\n",(long  int)output_tensor_data);
    //printf(" tensor2 data 0x%08lx\n",(long  int)output_tensor2_data);
    
  /*
    for(int i=0;i<sz && i<20;i++) printf("[%12d] %12.6f %12.6f\n",i,
            data[i],
            output_tensor_data[i]);
            //output_tensor2_data[i]);
            */

    /*
    FILE *F=fopen("out.dat","w");
    if( F==NULL )  {  printf("OOM!\n");exit(0); }
    int k=fwrite(output_tensor_data,sizeof(float),270*480*480,F);
    printf("done!  k=%d\n",k);
    fclose(F);
    */
      //h1=horloge();
      //printf("temps total %f sec\n",h1-h0);


  printf("output_tensor ndims = %d\n",0);

  onnx->ReleaseMemoryInfo(memory_info_cpu);
  onnx->ReleaseMemoryInfo(memory_info_cuda);

  //onnx->ReleaseValue(output_tensors[0]);
  //onnx->ReleaseValue(output2_tensors[0]);
  onnx->ReleaseValue(inTensor);
  free(data);
  return 0;
}



#endif
