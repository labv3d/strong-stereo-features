#ifndef MONONNX_H
#define MONONNX_H


#include "onnxruntime_c_api.h"


//  acces a l'API
extern const OrtApi*  onnx;

// pour les strings,  un peu partout
extern OrtAllocator* allocator;

// partage entre  toutes les sessions...
extern OrtEnv* env;
extern OrtSessionOptions* session_options;


//
// memory
//
extern OrtMemoryInfo* memory_info_cpu;
extern OrtMemoryInfo* memory_info_cuda;



#define TRY(expr)                             \
  do {                                                       \
    OrtStatus* status = (expr);                         \
    if (status != NULL) {                               \
      const char* msg = onnx->GetErrorMessage(status); \
      fprintf(stderr, "%s\n", msg);                          \
      onnx->ReleaseStatus(status);                     \
      abort();                                               \
    }                                                        \
  } while (0);





// retourne  l'heure en secondes,
double horloge();


//
// Init ONNX. Setup api
//
int initONNX(int useCuda);  //  return useCuda
void uninitONNX();

//
// lire un reseau.
// faire onnx->ReleaseSession(s) a la fin
//
OrtSession* readONNX(const char *name);


// info  sur  un  tenseur
void  dumpTensorInfo(OrtValue *tensor);

// donne les  dimensions  du tenseur name; dimensions fini  par -1
void getDimensions(OrtSession* session,const  char *name,int *dimensions);



// info sur une session
void dumpSessionInfo(OrtSession*  session);



#endif
