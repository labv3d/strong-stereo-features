// on  resize  les  images en  fonction  du tenseur d'entree du reseau?
#define RESIZE_IMAGE_MISMATCH


#define USE_COST_VOLUME

#include <assert.h>
#include <stdio.h>

#include "onnxruntime_c_api.h"

// pour les  options  de  session
#include "onnxruntime_session_options_config_keys.h"

#include  "mononnx.h"

// pour  cout
#include <iostream>


#include "opencv2/objdetect.hpp"
#include "opencv2/imgproc.hpp"
#include "opencv2/highgui.hpp"
#include "opencv2/videoio.hpp"
#include "opencv2/calib3d.hpp"
#include "opencv2/imgcodecs.hpp"

//
// stereo
//
// initialize values for StereoSGBM parameters
int numDisparities = 192;
int blockSize = 1;
int preFilterCap = 0;
int uniquenessRatio = 1;
int speckleRange = 2;
int speckleWindowSize = 45; // 0=deactivate  filter
int disp12MaxDiff = 16;
int minDisparity = 0;
int p1 = 512;
int p2 = 4096;


/**
 * *** ATTENTION RGB vs BGR!!!!! ***
 * convert input from HWC format to CHW format
 * \param input A single image. The byte array has length of 3*h*w
 * \param h image height
 * \param w image width

 * \param output A float array. should be freed by caller after use
 * \param output_count Array length of the `output` param
 *
 * return  number of bytes
 */
int yxc_to_cyx(const uint8_t* input, int h, int w, int c, float** pdata,float factor) {
  int stride=h*w;
  float *data= *pdata;
  if( data==NULL ) {
      printf("Allocating tensor data\n");
      data = (float*)malloc(stride*c*sizeof(float));
  }else{
      printf("Reusing tensor data\n");
  }
  if( c==1 )  {
      for (int i = 0; i < stride; i++) {
          data[i] = input[i]/255.0;
      }
  }else{
      for (int i = 0; i < stride; i++) {
        for (int j = 0; j <c; j++) {
          data[j * stride + i] = input[i * c + j]*factor;
        }
      }
  }
  *pdata = data;
  return(stride*c*sizeof(float));
}


int cyx_to_yxc(float *tdata,int c,int h,int w,uint8_t *data,float factor,float dc) {
    float min=tdata[0];
    float max=tdata[0];
    for(int j=0;j<c;j++) {
        for(int i=0;i<h*w;i++) {
            float f=tdata[j*h*w+i];
            if( f<min  )  min=f;
            if( f>max )  max=f;
            int  v=(int)(f*factor+dc+0.5);
            data[i*c+j]=(unsigned char)((v<0)?0:(v>255?255:v));
        }
    }
    printf("MIN %12.6f MAX %12.6f\n",min,max);
    return(0);
}



int readImage(const char* name,cv::Mat &frame, int idealCols,int  idealRows) {
    printf("reading %s\n",name);
    frame=cv::imread(name, -1);
    if( frame.empty() ) {
        printf("unable to  read %s\n",name);
        return(-1);
    }
    printf("Input  image is %d x %d c=%d depth=0x%x %x %x  continu=%d\n",frame.cols,frame.rows,frame.channels(),frame.depth(),CV_8U,CV_16U,frame.isContinuous());

    if( frame.cols!=idealCols || frame.rows!=idealRows ) {
#ifdef RESIZE_IMAGE_MISMATCH
        printf("resize image from %d x %d to %d x %d.\n",frame.cols,frame.rows,idealCols,idealRows);
        cv::resize(frame,frame,cv::Size(idealCols,idealRows));
#else
        printf("Image size  does  not  fit  desired  tensor  size\n");
        exit(0);
#endif
    }

    return(0);
}

int image2tensor(cv::Mat frame,OrtValue **ptensor,float factor) {
    // attention au 1 ici...  pas toujours necessaire, mais pour les conv
    int64_t shape[] = {1,frame.channels(),frame.rows,frame.cols};
    int sz=(int)(shape[1]*shape[2]*shape[3]);
    printf("shape %d %d %d %d  sz=%d\n",(int)shape[0],(int)shape[1],(int)shape[2],(int)shape[3],sz);

    float *data;
    int nbBytes;

    if( *ptensor==NULL )  {
        data=NULL;
    }else{
        // utilise  le data  du  tenseur
        TRY(onnx->GetTensorMutableData(*ptensor, (void**)&data));
    }

    // depth()  possible  values
    // CV_8U - 8-bit unsigned integers ( 0..255 )
    // CV_8S - 8-bit signed integers ( -128..127 )
    // CV_16U - 16-bit unsigned integers ( 0..65535 )
    // CV_16S - 16-bit signed integers ( -32768..32767 )
    // CV_32S - 32-bit signed integers ( -2147483648..2147483647 )
    // CV_32F - 32-bit floating-point numbers ( -FLT_MAX..FLT_MAX, INF, NAN )
    // CV_64F - 64-bit floating-point numbers ( -DBL_MAX..DBL_MAX, INF, NAN ) 

    if( frame.depth()==CV_8U )  {
        nbBytes=yxc_to_cyx(frame.data,frame.rows,frame.cols,frame.channels(), &data,factor);
    }else{
        printf("unsupported image  data type\n");
        return(-1);
    }

    if( *ptensor==NULL  ) {
        TRY(onnx->CreateTensorWithDataAsOrtValue(memory_info_cpu,data,nbBytes, shape,4, ONNX_TENSOR_ELEMENT_DATA_TYPE_FLOAT, ptensor));
    }

    return(0);
}

int tensor2image(OrtValue *tensor,cv::Mat &frame,float factor) {
    OrtTypeInfo* typeinfo;
    TRY(onnx->GetTypeInfo(tensor, &typeinfo));
    if( typeinfo==NULL )  {  printf("no typeinfo\n");return(-1);  }

    const  OrtTensorTypeAndShapeInfo* ti;
    TRY(onnx->CastTypeInfoToTensorInfo(typeinfo, &ti));
    ONNXTensorElementDataType type;
    size_t ndims;
    int64_t dims[50];
    TRY(onnx->GetTensorElementType(ti, &type));
    TRY(onnx->GetDimensionsCount(ti, &ndims))
    TRY(onnx->GetDimensions(ti, dims, ndims));
    printf("           : tensor dims=%d [",(int)ndims);
    for(int j=0;j<(int)ndims;j++)  printf("%d,",(int)dims[j]);
    onnx->ReleaseTypeInfo(typeinfo);

    printf("]  type=%d (FLOAT=%d,int64=%d,...)\n",type,
            ONNX_TENSOR_ELEMENT_DATA_TYPE_FLOAT,
            ONNX_TENSOR_ELEMENT_DATA_TYPE_INT64);
    if( ndims!=4  ) {  printf("Expected 4 dims... got %d\n",(int)ndims);return(-1);}
    if( type!= ONNX_TENSOR_ELEMENT_DATA_TYPE_FLOAT  ) {
        printf("Only type float is  supported.\n");return(-1);
    }

    float *tdata;
    TRY(onnx->GetTensorMutableData(tensor, (void**)&tdata));

    if( dims[0]!=1 ) { printf("dimension  0  should be  1!!\n");return(-1); }

    // suppose  cyx
    if( dims[1]==3 ) {
        frame.create((int)dims[2],(int)dims[3],CV_8UC3);
        printf("frame data is 0x%08lx\n",(long  int)frame.data);
        cyx_to_yxc(tdata,frame.channels(),frame.rows,frame.cols,frame.data,factor,0.0);
    }else{
        printf("Only support rgb images  (3xHxW)\n");return(-1);
    }
    return(0);
}

int tensor2Cvol(OrtValue *tensor,cv::Mat &cvol) {
    OrtTypeInfo* typeinfo;
    TRY(onnx->GetTypeInfo(tensor, &typeinfo));
    if( typeinfo==NULL )  {  printf("no typeinfo\n");return(-1);  }

    const  OrtTensorTypeAndShapeInfo* ti;
    TRY(onnx->CastTypeInfoToTensorInfo(typeinfo, &ti));
    ONNXTensorElementDataType type;
    size_t ndims;
    int64_t dims[50];
    TRY(onnx->GetTensorElementType(ti, &type));
    TRY(onnx->GetDimensionsCount(ti, &ndims))
    TRY(onnx->GetDimensions(ti, dims, ndims));
    printf("           : tensor dims=%d [",(int)ndims);
    for(int j=0;j<(int)ndims;j++)  printf("%d,",(int)dims[j]);
    onnx->ReleaseTypeInfo(typeinfo);

    printf("]  type=%d (FLOAT=%d,int64=%d,...)\n",type,
            ONNX_TENSOR_ELEMENT_DATA_TYPE_FLOAT,
            ONNX_TENSOR_ELEMENT_DATA_TYPE_INT64);
    if( ndims!=4  ) {  printf("Expected 4 dims... got %d\n",(int)ndims);return(-1);}
    if( type!= ONNX_TENSOR_ELEMENT_DATA_TYPE_FLOAT  ) {
        printf("Only type float is  supported.\n");return(-1);
    }

    float *tdata;
    TRY(onnx->GetTensorMutableData(tensor, (void**)&tdata));

    int dimsC[] = { (int)dims[1],(int)dims[2],numDisparities };
    cvol.create(3, dimsC, CV_16S);
    short *p=(short *)cvol.data;
    int nr=cvol.size[0];
    int nc=cvol.size[1];
    int nd=cvol.size[2];

    //... a finir ...

    // cost[r,c,d]=perm[r,c,c-d]
    float *t=tdata;
    unsigned short min=65535,max=0;
    unsigned short *q=(unsigned  short *)cvol.data;
    for(int y=0;y<nr;y++,t=t+nc*nc,p=p+nc*nd) {
        for(int x=0;x<nc;x++) {
            for(int  d=0;d<nd;d++ ) {
                int xx=x-d;
                if( xx>=0 && xx<nc ) {
                    unsigned short s=(int)((1.0-t[x*nc+xx])*1023.0+0.5);
                    p[x*nd+d]=s;
                    if(  s<min  ) min=s;
                    if(  s>max )  max=s;
                }
            }
        }
    }
    printf("*** min=%d  max=%d\n",min,max);

    return(0);
}

//
// prend un chanel (part)  et fait  une  image mono
//
int tensorPart2image(OrtValue *tensor,int part,cv::Mat &frame,float factor,float dc) {
    OrtTypeInfo* typeinfo;
    TRY(onnx->GetTypeInfo(tensor, &typeinfo));
    if( typeinfo==NULL )  {  printf("no typeinfo\n");return(-1);  }

    const  OrtTensorTypeAndShapeInfo* ti;
    TRY(onnx->CastTypeInfoToTensorInfo(typeinfo, &ti));
    ONNXTensorElementDataType type;
    size_t ndims;
    int64_t dims[50];
    TRY(onnx->GetTensorElementType(ti, &type));
    TRY(onnx->GetDimensionsCount(ti, &ndims))
    TRY(onnx->GetDimensions(ti, dims, ndims));
    printf("           : tensor dims=%d [",(int)ndims);
    for(int j=0;j<(int)ndims;j++)  printf("%d,",(int)dims[j]);
    onnx->ReleaseTypeInfo(typeinfo);

    printf("]  type=%d (FLOAT=%d,int64=%d,...)\n",type,
            ONNX_TENSOR_ELEMENT_DATA_TYPE_FLOAT,
            ONNX_TENSOR_ELEMENT_DATA_TYPE_INT64);
    if( ndims!=4  ) {  printf("Expected 4 dims... got %d\n",(int)ndims);return(-1);}
    if( type!= ONNX_TENSOR_ELEMENT_DATA_TYPE_FLOAT  ) {
        printf("Only type float is  supported.\n");return(-1);
    }

    float *tdata;
    TRY(onnx->GetTensorMutableData(tensor, (void**)&tdata));

    printf("tensor data  at 0x%08lx\n",(long  int)tdata);

    if( dims[0]!=1 ) { printf("dimension  0  should be  1!!\n");return(-1); }

    // suppose  cyx
    frame.create((int)dims[2],(int)dims[3],CV_8UC1);
    printf("frame data is 0x%08lx\n",(long  int)frame.data);
    cyx_to_yxc(tdata+part*dims[2]*dims[3],frame.channels(),frame.rows,frame.cols,frame.data,factor,dc);
    return(0);
}



void genCostVol(cv::Mat &imgL,cv::Mat &imgR, cv::Mat &cvol) {
    int dims[] = { imgL.rows,imgL.cols,64 };
    cvol.create(3, dims, CV_16S);
    short *p=(short *)cvol.data;
    int nr=cvol.size[0];
    int nc=cvol.size[1];
    int nd=cvol.size[2];
    printf("dims  are %d,%d,%d\n",nr,nc,nd);

    // cost[r,c,d]=img1[r,c]-img2[r-d,c]
    int nz=imgL.channels();
    unsigned char *pL=(unsigned  char *)imgL.data;
    unsigned char *pR=(unsigned  char *)imgR.data;
    p=(short *)cvol.data;
    short  max=0;
    for(int r=0;r<nr;r++)  {
        for(int c=0;c<nc;c++)  {
            unsigned char *vL=pL+((r*nc+c)*nz);
            for(int d=0;d<nd;d++)  {
                int cR=c-d;
                if( cR<0 ) cR=0;
                unsigned char *vR=pR+((r*nc+cR)*nz);
                short cost=0;
                for(int z=0;z<nz;z++) {
                    short t=((short)vR[z]-(short)vL[z]);
                    if(t<0) t=-t;
                    cost+=t;
                }
                if(cost>max)max=cost;
                p[(r*nc+c)*nd+d]=cost;
            }
        }
    }
    printf("MAX is %d\n",max);
}

static void usage(char *prog) {
    printf("usage: %s -net blub.net \n",prog?prog:"");
    printf("      -left /data/datasets/KITTI2015/testing/image_2/%%06d_10.png\n");
    printf("      -right /data/datasets/KITTI2015/testing/image_2/%%06d_10.png\n");
    printf("      -out out%%04d\n");
    printf("      -seq 0 19\n");
    printf("      -cuda [-nocuda] [-display]\n");
    printf("      -sgbm-ndisp 128\n");
    printf("      [-autocrop]  (crop a gauche la disparite pour matcher l'image)\n");
    exit(0);
}

//
// filter -n net.onnx  -i  input.png  -o  output.png  -cuda
//
int main(int argc, char* argv[]) {
    int cuda;
    char  leftName[200];
    char  rightName[200];
    char  outName[200];
    int iFrom=0;
    int iTo=0;
    int display=0;  //  affiche?
    char netName[200];
    int  autocrop=0;

    cuda=1;  // par  defaut  :  on
    strcpy(leftName,"../../img/left-01.png");
    strcpy(rightName,"../../img/right-01.png");
    strcpy(outName,"out");
    strcpy(netName,"../../modeles/testonnx.onnx");

    for(int i=1;i<argc;i++) {
        if(  strcmp("-h",argv[i])==0 )  usage(argv[0]);
        if(  strcmp("-net",argv[i])==0  &&  i+1<argc )  {
            strcpy(netName,argv[i+1]);
            i++;continue;
        }
        if(  strcmp("-left",argv[i])==0  &&  i+1<argc )  {
            strcpy(leftName,argv[i+1]);
            i++;continue;
        }
        if(  strcmp("-right",argv[i])==0  &&  i+1<argc )  {
            strcpy(rightName,argv[i+1]);
            i++;continue;
        }
        if(  strcmp("-out",argv[i])==0  &&  i+1<argc )  {
            strcpy(outName,argv[i+1]);
            i++;continue;
        }
        if(  strcmp("-seq",argv[i])==0  &&  i+2<argc )  {
            iFrom=atoi(argv[i+1]);
            iTo=atoi(argv[i+2]);
            i+=2;continue;
        }

        if(  strcmp("-autocrop",argv[i])==0  )  {
            autocrop=1;
            continue;
        }
        if(  strcmp("-sgbm-ndisp",argv[i])==0  &&  i+1<argc )  {
            numDisparities = atoi(argv[i+1]);
            i+=1;continue;
        }

        if(  strcmp("-display",argv[i])==0 )  { display=1; continue; }
        if(  strcmp("-cuda",argv[i])==0 )  { cuda=1; continue; }
        if(  strcmp("-nocuda",argv[i])==0 )  { cuda=0; continue; }
        printf("Unkown  option  %s\n",argv[i]);
        usage(argv[0]);
    }

    if( leftName[0]==0 ||  rightName[0]==0 )  {  printf("need images!  [-i]\n"); usage(argv[0]);  }

    initONNX(cuda);

    // Creating an object of StereoSGBM algorithm
    cv::Ptr<cv::StereoSGBM> stereo = cv::StereoSGBM::create();

    stereo->setNumDisparities(numDisparities);
    stereo->setBlockSize(blockSize);
    stereo->setPreFilterCap(preFilterCap);
    stereo->setUniquenessRatio(uniquenessRatio);
    stereo->setSpeckleRange(speckleRange);
    stereo->setSpeckleWindowSize(speckleWindowSize);
    stereo->setDisp12MaxDiff(disp12MaxDiff);
    stereo->setMinDisparity(minDisparity);
    stereo->setP1(p1);
    stereo->setP2(p2);

    cv::Mat  cvol;  // cost volume


    cv::Mat frameL;
    cv::Mat frameR;
    OrtValue* inLTensor=NULL;
    OrtValue* inRTensor=NULL;
    cv::Mat outI;

    size_t nbOut;
    OrtValue** outTensors=NULL;

    OrtSession* session;
    session=readONNX(netName);
    dumpSessionInfo(session);

    int dimL[10];
    int dimR[10];
    getDimensions(session,"iLeft",dimL);
    getDimensions(session,"iRight",dimR);
    printf("iLeft: ");
    for(int i=0;dimL[i]>=0;i++) printf("[%d]",dimL[i]);
    printf("\n");
    printf("iRight: ");
    for(int i=0;dimR[i]>=0;i++) printf("[%d]",dimR[i]);
    printf("\n");

    OrtIoBinding* binding;
    TRY(onnx->CreateIoBinding(session, &binding));

    //// GO!
    cv::Mat disp;
    cv::Mat imgVide;

    char  bufL[200];
    char  bufR[200];
    char  bufO[200];

    double h1,h2,h3,h4,h5;
    for(int i=iFrom;i<=iTo;i++)  {
        h1=horloge();

        sprintf(bufL,leftName,i);
        sprintf(bufR,rightName,i);
        sprintf(bufO,outName,i);

        if( readImage(bufL,frameL,dimL[3],dimL[2]) ) {  printf("unable  to  read\n");exit(0); }
        if( readImage(bufR,frameR,dimR[3],dimR[2]) ) {  printf("unable  to  read\n");exit(0); }

        h2=horloge();
        image2tensor(frameL,&inLTensor,1.0/255.0);
        image2tensor(frameR,&inRTensor,1.0/255.0);

        // inference
        onnx->ClearBoundInputs(binding);
        onnx->ClearBoundOutputs(binding);

        TRY(onnx->BindInput(binding, "iLeft", inLTensor));
        TRY(onnx->BindInput(binding, "iRight", inRTensor));
        TRY(onnx->BindOutputToDevice(binding,"Output", memory_info_cpu));

        h3=horloge();
        // run le reseau
        TRY(onnx->RunWithBinding(session,NULL,binding));


        // va chercher le resultat
        TRY(onnx->GetBoundOutputValues(binding,allocator, &outTensors,&nbOut));

        //dumpTensorInfo(outTensors[0]);

        //tensor2image(outTensors[0],outI,255.0);
        
        h4=horloge();

        //genCostVol(frame1,frame2, cvol);
        tensor2Cvol(outTensors[0],cvol);

        onnx->ReleaseValue(outTensors[0]);


#ifdef USE_COST_VOLUME
        stereo->setMode(cv::StereoSGBM::MODE_NN);  // CostVolume!
        stereo->setCostVolume(cvol);

        stereo->compute(imgVide,imgVide,disp);

#else
        stereo->setMode(cv::StereoSGBM::MODE_SGBM);
        stereo->compute(frame1,frame2,disp);
#endif
        h5=horloge();

        if( autocrop ) {
            if( disp.cols>dimL[3] ) {
                printf("Autocrop disparity from %d to %d\n",disp.cols,dimL[3]);
                int  k=disp.cols-dimL[3];
                disp = disp(cv::Range(0,disp.rows), cv::Range(k,disp.cols));
                printf("new  size is  %d x %d\n",disp.cols,disp.rows);
            }
        }

        if( disp.empty() ) exit(0);

        // NOTE: Code returns a 16bit signed single channel image,
        // CV_16S containing a disparity map scaled by 16. Hence it

        // on sauvegarde la disparite *64, en short unsigned
        cv::Mat tmp;

        disp.convertTo(tmp,CV_16U, 4.0); //  disp*4  -> disparity * 64
        char buf[200];
        strcpy(buf,bufO);strcat(buf,"-disp.png");
        printf("Save disparity to %s\n",buf);
        imwrite(buf,tmp);  // disparity*64, as 16bit image

        disp.convertTo(tmp,CV_8U, 255.0/16.0/numDisparities); //  disp/16/numd*65535  -> disparity/numd*255
        strcpy(buf,bufO);strcat(buf,"-display.png");
        printf("Save disparity (display) to %s\n",buf);
        imwrite(buf,tmp);  // disparity/numD*255

        if( display  ) {
            // Scaling down the disparity values and normalizing them
            cv::Mat result;
            disp.convertTo(result,CV_8U,4.0/32.0); //  [0..4*32]*64  -> 0..256

            cv::imshow("disparity",result);
            if( cv::waitKey(1)==27 ) break;
            //cv::waitKey(0);
        }

        printf("temps %12.6f %12.6f %12.6f %12.6f\n",h2-h1,h3-h2,h4-h3,h5-h4);
    }



    onnx->ReleaseSession(session);
    uninitONNX();

    return 0;
}



